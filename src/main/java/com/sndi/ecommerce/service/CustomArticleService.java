package com.sndi.ecommerce.service;

import javax.persistence.EntityNotFoundException;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.sndi.ecommerce.entities.Article;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
@Profile("dev")
public class CustomArticleService implements IArticleService{

	@Override
	public Article createArticle(DtoArticle dto) {
		
		throw new RuntimeException("Functionality not implemented yet");
	}

	@Override
	public Article findArticle(Long id) {
		
		throw new EntityNotFoundException("Requested article does not exist");
	}

}
