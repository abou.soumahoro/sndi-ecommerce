package com.sndi.ecommerce.service;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.sndi.ecommerce.entities.Article;
import com.sndi.ecommerce.entities.Vendeur;
import com.sndi.ecommerce.repositories.ArticleRepository;
import com.sndi.ecommerce.repositories.UserRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
@Profile("prod")
public class ArticleService implements IArticleService{

	private final ArticleRepository articleRepository;
	private final UserRepository userRepository;
	
	public Article findArticle(Long id) {
		return articleRepository.findById(id).get();
	}
	
	public Article createArticle(DtoArticle dto) {
		Article article =  new Article();
		article.setName(dto.name);
		article.setDescription(dto.desc);
		article.setPrice(dto.price);
		Vendeur vendeur = (Vendeur) userRepository.findById(dto.vendeur_id).get();
		article.setVendeur(vendeur);
		
		return articleRepository.save(article);
	}



}
