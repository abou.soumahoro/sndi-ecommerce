package com.sndi.ecommerce.security;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.sndi.ecommerce.security.jwt.JwtTokenFilter;

@Configuration
@EnableWebSecurity
public class SecurityConfig{

	
	private UserDetailsService userDetailsService;
	private JwtTokenFilter jwtTokenFilter;
	private AuthenticationEntryPoint authenticationEntryPoint;
	
	@Autowired
	public SecurityConfig(UserDetailsService detailsService, JwtTokenFilter tokenFilter, AuthenticationEntryPoint entryPoint) {
		this.userDetailsService = detailsService;
		this.jwtTokenFilter =  tokenFilter;
		this.authenticationEntryPoint = entryPoint;
	}
	
	@Bean
	public SecurityFilterChain filterChain(HttpSecurity httpSecurity) throws Exception{
		
		//Enable CORS and disable CSRF
		httpSecurity = disableCsrfForCors(httpSecurity);
		httpSecurity = setSessionToStateless(httpSecurity);
		httpSecurity = setUnAuthorizeRequestHandler(httpSecurity);
		httpSecurity = addJwtTokenFilter(httpSecurity);
		httpSecurity = setPermissionOnEnpoinds(httpSecurity);
		
		return httpSecurity.build();
	}
	
	@Bean
	public AuthenticationManager autManager(HttpSecurity httpSecurity) throws Exception {
		
		return httpSecurity.getSharedObject(AuthenticationManagerBuilder.class)
						 .userDetailsService(userDetailsService)
						 .passwordEncoder(passwordEncoder())
						 .and()
						 .build();
	}
	
	@Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
	
	
	
	private HttpSecurity disableCsrfForCors(HttpSecurity httpSecurity) throws Exception {
		return httpSecurity.cors().and().csrf().disable();
	}
	
	private HttpSecurity setSessionToStateless(HttpSecurity httpSecurity) throws Exception {
		return httpSecurity.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and();
	}
	
	
	private HttpSecurity setUnAuthorizeRequestHandler(HttpSecurity httpSecurity) throws Exception {
		
		return httpSecurity
			.exceptionHandling()
			.authenticationEntryPoint(authenticationEntryPoint)
			.and();
	}
	
	private HttpSecurity setPermissionOnEnpoinds(HttpSecurity httpSecurity) throws Exception {
		return httpSecurity
				.authorizeHttpRequests()
				.antMatchers("/api/v1/auth/**","/home","/aprpopos")
				.permitAll()
				.anyRequest().authenticated()
				.and();
		
	}
	
	private HttpSecurity addJwtTokenFilter(HttpSecurity httpSecurity) {
		
		return httpSecurity.addFilterBefore(jwtTokenFilter, UsernamePasswordAuthenticationFilter.class);
	}
}
