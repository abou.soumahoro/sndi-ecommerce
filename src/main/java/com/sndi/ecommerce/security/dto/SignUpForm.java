package com.sndi.ecommerce.security.dto;


import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public final class SignUpForm {

	  	@NotNull
	  	@NotEmpty
	    @Size(min = 3, max = 50)
	    private String firstName;
	 
	  	@NotNull(message = "lastName cannot be null")
	  	@NotEmpty(message = "lastName cannot be empty")
	    @Size(min = 3, max = 50)
	    private String lastName;
	
	    
	    @Size(max = 60)
	    private String email;
	    
	    @NotNull(message = "password cannot be null")
	  	@NotEmpty(message = "password cannot be null")
	    @Size(min = 6, max = 40)
	    private String password;
	    
	    @NotNull(message = "telephone cannot be null")
	  	@NotEmpty(message = "telephone cannot be null")
	    @Size(min = 8, max = 12)
	    private String telephone;
	    
	    private String role;
	    
	    
}
