package com.sndi.ecommerce.security.jwt;


import javax.servlet.http.HttpServletRequest;

import org.springframework.security.core.Authentication;


public interface TokenProvider {

	public boolean validateToken(String token);
	public  String getUsernameFromToken(String token);
	public String generateToken(Authentication authentication);
	public String getJwt(HttpServletRequest request);
	
}
