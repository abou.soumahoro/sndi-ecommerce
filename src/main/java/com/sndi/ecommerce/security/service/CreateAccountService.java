package com.sndi.ecommerce.security.service;


import org.springframework.stereotype.Service;

import com.sndi.ecommerce.entities.Role;
import com.sndi.ecommerce.entities.User;
import com.sndi.ecommerce.security.AccountCreationException;
import com.sndi.ecommerce.security.Security;
import com.sndi.ecommerce.security.dto.SignUpForm;
import com.sndi.ecommerce.security.repo.UserJpaRepository;
import com.sndi.ecommerce.security.repo.UserPersistanceRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class CreateAccountService{

	
	private final UserJpaRepository accounrRepository;
	private final Security security;
	
	public User createAccount(final SignUpForm request){
		
		if(accounrRepository.existsByTelephone(request.getTelephone())) {
			throw new AccountCreationException("Le numéro de télephone est déja utilisé par un autre utilisateur");
		}
		
		String encodedPassword = security.cryptPassword(request.getPassword());
		User user = new User(
					null,
					request.getFirstName(),
					request.getLastName(),
					request.getTelephone(),
					Role.valueOf(request.getRole()),
					encodedPassword
				);
		
		
		user  = accounrRepository.save(user);
		
		return user;
	}
	
}
