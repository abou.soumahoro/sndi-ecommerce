package com.sndi.ecommerce.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sndi.ecommerce.entities.Article;

public interface ArticleRepository extends JpaRepository<Article, Long>{


}
