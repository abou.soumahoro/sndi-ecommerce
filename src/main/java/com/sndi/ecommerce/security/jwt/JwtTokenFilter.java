package com.sndi.ecommerce.security.jwt;


import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.sndi.ecommerce.security.SecurityHelper;

@Component
public class JwtTokenFilter extends OncePerRequestFilter{

	
	private TokenProvider tokenProvider;
	private SecurityHelper securityHelper;
	
	static final String TOKEN_PREFIX = "Bearer ";
	
	public JwtTokenFilter(TokenProvider tokenProvider, SecurityHelper securityHelper) {
		this.tokenProvider = tokenProvider;
		this.securityHelper = securityHelper;
		
	}
	
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		
		String jwt = tokenProvider.getJwt(request);
		
		if(jwt != null && tokenProvider.validateToken(jwt)) {
			setAuthenticationContext(jwt, request);
		}
		
		filterChain.doFilter(request, response);
	}
	
	
	private void setAuthenticationContext(String jwt, HttpServletRequest request) {
		
		String userName = tokenProvider.getUsernameFromToken(jwt);
		UsernamePasswordAuthenticationToken authentication =  securityHelper.setAuthenticationToken(userName, request);
		SecurityContextHolder
			.getContext()
			.setAuthentication(authentication);
	}

	
	
}
