package com.sndi.ecommerce.web;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.sndi.ecommerce.entities.Article;
import com.sndi.ecommerce.service.ArticleService;
import com.sndi.ecommerce.service.DtoArticle;
import com.sndi.ecommerce.service.IArticleService;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
public class ArticleController {

	private final IArticleService articleService;
	
	@GetMapping("/articles/{idArticle}")
	public Article getArticle(@PathVariable Long idArticle) {
		
		return articleService.findArticle(idArticle);
	}
	
	@PostMapping("/articles")
	public Article createArticle(@RequestBody DtoArticle dtoArticle) {
		return articleService.createArticle(dtoArticle);
	}
	
}
