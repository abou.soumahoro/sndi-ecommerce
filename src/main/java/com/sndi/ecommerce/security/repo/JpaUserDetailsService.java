package com.sndi.ecommerce.security.repo;


import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.sndi.ecommerce.entities.User;
import com.sndi.ecommerce.security.service.UserPrinciple;


@Service
public class JpaUserDetailsService implements UserDetailsService{

	
	UserJpaRepository repository;
	
	@Autowired
	public JpaUserDetailsService(UserJpaRepository repository) {
		this.repository = repository;
	}
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		User user = repository.getByTelephone(username);
		if(user == null)
			throw new EntityNotFoundException("User Not Found with -> username or email : "+   username);
		
		
		return UserPrinciple.build(user);
	}

}
