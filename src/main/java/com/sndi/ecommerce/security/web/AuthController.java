package com.sndi.ecommerce.security.web;


import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sndi.ecommerce.entities.User;
import com.sndi.ecommerce.security.dto.LoginForm;
import com.sndi.ecommerce.security.dto.SignUpForm;
import com.sndi.ecommerce.security.jwt.TokenProvider;
import com.sndi.ecommerce.security.service.CreateAccountService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping(path = "/api/v1/auth")
@RequiredArgsConstructor
public class AuthController {
	
	
	private final CreateAccountService accountService;
	private final AuthenticationManager authenticationManager;
	private final TokenProvider tokenProvider;
	private static final Logger LOGGER = LoggerFactory.getLogger(AuthController.class);


	@PostMapping("/signUp")
	public ResponseEntity<User> createAccount(@RequestBody@Valid final SignUpForm signUpForm, BindingResult bindingResult){
		
		LOGGER.info("Account creation - User: {}", signUpForm.getFirstName().concat(" ").concat(signUpForm.getLastName()));
		
		HttpRequestUtil.checkBindingResult(bindingResult);
		User u = this.accountService.createAccount(signUpForm);
		
		LOGGER.info("Account creation - Account {} created successfully ", signUpForm.getTelephone());
		
		return new ResponseEntity<User>(u, HttpStatus.CREATED);
	}
	
	@PostMapping("/signIn")
	public ResponseEntity<Object> login(@RequestBody@Valid LoginForm loginForm, BindingResult bindingResult){
		
		LOGGER.info("User login - Connecting user {}", loginForm.getTelephone() );

		HttpRequestUtil.checkBindingResult(bindingResult);
		Authentication authentication = authenticationManager
					.authenticate(new UsernamePasswordAuthenticationToken(loginForm.getTelephone(), loginForm.getPassword()));
		
		LOGGER.info("Authentication successful for user - {}", loginForm.getTelephone());
		
		SecurityContextHolder.getContext().setAuthentication(authentication);
		String token = tokenProvider.generateToken(authentication);
		
		return ResponseEntity
					.ok()
					.header(HttpHeaders.AUTHORIZATION, token)
					.body(authentication.getPrincipal());
	}
	

	
	
	
	
	
}
