package com.sndi.ecommerce.security.service;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sndi.ecommerce.entities.User;

import lombok.AllArgsConstructor;
import lombok.Data;


public class UserPrinciple implements UserDetails{
	
	 private Long id;
	  
	 private String name;
	  
	 private String telephone;
	  
	 
	 private Collection<? extends GrantedAuthority> authorities;
	 
	@JsonIgnore private String password;

	
	public UserPrinciple(long id, String name, String username, List<GrantedAuthority> authorities, String password) {
		this.id = id;
		this.name = name;
		this.telephone = username;
		this.password = password;
		this.authorities = authorities;
	}

	public static UserDetails build(User user) {
		List<GrantedAuthority> authorities = Stream.of(user.getRole())
					.map(role -> new SimpleGrantedAuthority(role.getRole()) )
					.collect(Collectors.toList());
		
		String name = user.getName() + " "+ user.getSurname();
		UserPrinciple userPrinciple = new UserPrinciple(user.getId(), name, user.getTelephone(), authorities, user.getPassword());
		
		return userPrinciple;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getPassword() { return password; }

	@Override
	public String getUsername() { return telephone; }
		
	@Override
	public boolean isAccountNonExpired() { return true; }
		
	@Override
	public boolean isAccountNonLocked() { return true;}
		
	@Override
	public boolean isCredentialsNonExpired() { return true;}
		
	@Override
	public boolean isEnabled() { return true;}
	
	@Override 
	public boolean equals(Object o) { 
		if (this == o) { return true; }
		
		if (o == null || getClass() != o.getClass()) { return false; }
		
		UserPrinciple user = (UserPrinciple) o; 
		
		return Objects.equals(id, user.id); 
		
	}
		
	

}
