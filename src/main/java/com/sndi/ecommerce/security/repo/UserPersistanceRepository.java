package com.sndi.ecommerce.security.repo;

import com.sndi.ecommerce.entities.User;

public interface UserPersistanceRepository {

	long save(User user);
	
	//boolean existsByEmail(String email);

	boolean existsByTelephone(String telephone);

	
}
