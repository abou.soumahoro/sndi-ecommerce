package com.sndi.ecommerce.security.repo;


import org.springframework.data.jpa.repository.JpaRepository;

import com.sndi.ecommerce.entities.User;


public interface UserJpaRepository extends JpaRepository<User, Long>{

	//boolean existsByEmail(String email);

	boolean existsByTelephone(String telephone);

	User getByTelephone(String indentificationCode);

	User findByTelephone(String telephone);

}
