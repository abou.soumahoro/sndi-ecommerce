package com.sndi.ecommerce.security;


public interface Security {

	String cryptPassword(String password);
}
