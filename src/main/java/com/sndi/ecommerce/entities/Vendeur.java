package com.sndi.ecommerce.entities;

import java.util.Set;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@DiscriminatorValue(value = "V")
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Vendeur extends User{

	@OneToMany
	@Getter
	@Setter
	private Set<Article> articles;
	
}
