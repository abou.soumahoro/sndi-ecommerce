package com.sndi.ecommerce.security;

import org.springframework.security.core.context.SecurityContextHolder;

public class SecurityUtil {

	public static String getUserPrincipal() {
		return SecurityContextHolder.getContext().getAuthentication().getName();
	}

}
