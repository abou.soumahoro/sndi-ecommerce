package com.sndi.ecommerce.service;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class DtoArticle {

	public String name;
	public String desc;
	public double price;
	public Long vendeur_id;
	
	
}
