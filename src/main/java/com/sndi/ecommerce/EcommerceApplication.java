package com.sndi.ecommerce;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;

import com.sndi.ecommerce.entities.Article;
import com.sndi.ecommerce.entities.User;
import com.sndi.ecommerce.entities.Vendeur;
import com.sndi.ecommerce.repositories.ArticleRepository;
import com.sndi.ecommerce.repositories.UserRepository;
import com.sndi.ecommerce.service.ArticleService;
import com.sndi.ecommerce.service.DtoArticle;
import com.sndi.ecommerce.service.IArticleService;

@SpringBootApplication
public class EcommerceApplication implements CommandLineRunner{
	
	@Autowired
	UserRepository userRepository;
	@Autowired
	IArticleService articleService;
	@Autowired
	Environment environment;
	
	public static void main(String[] args) {
		SpringApplication.run(EcommerceApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		
		User vendeur =  new Vendeur();
		vendeur.setName("Soum");
		vendeur.setSurname("Abou");
		vendeur.setTelephone("1234");
		userRepository.save(vendeur);
		
		System.out.println(environment.getProperty("spring.profiles.active"));
		
		/*DtoArticle article = new DtoArticle("Phone", "IPHONE", 12000D, vendeur.getId());
		articleService.createArticle(article);*/
		
		
	}

}
