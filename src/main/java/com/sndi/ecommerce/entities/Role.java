package com.sndi.ecommerce.entities;


public enum Role {

	ROLE_VENDEUR("Vendeur"), ROLE_ACHETEUR("Acheteur"), ROLE_LIVREUR("Livreur");
	private String role;
	
	private Role(String role) {
		this.role = role;
	}
	
	
	public String getRole() {
	
		return this.role;
	}
}
