package com.sndi.ecommerce.service;

import com.sndi.ecommerce.entities.Article;

public interface IArticleService {

	Article createArticle(DtoArticle dto);
	Article findArticle(Long id);
}
